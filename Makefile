.PHONY : all clean help  jtestdft jtestdft-init jtestdft-build jtestdft-link
#-- build all
all:  jtestdft jtestdft-init jtestdft-build jtestdft-link
#-- build jtestdft
jtestdft: jtestdft-init jtestdft-build jtestdft-link

# ==== jtestdft

#-- jtestdft: initialize for debug build
jtestdft-init:
	@mkdir -p debug
	@mkdir -p debug/jtestdft-dir/src
	@mkdir -p debug/jtestdft-dir/home/arrizza/projects/cpip/cpip/logger

-include debug/jtestdft-dir/src/main.cpp.d
debug/jtestdft-dir/src/main.cpp.o: src/main.cpp
	c++ -MMD -c "-Isrc" "-I/home/arrizza/projects/cpip"  -std=c++20 -D_GNU_SOURCE  src/main.cpp -o debug/jtestdft-dir/src/main.cpp.o
-include debug/jtestdft-dir/home/arrizza/projects/cpip/cpip/logger/logger.cpp.d
debug/jtestdft-dir/home/arrizza/projects/cpip/cpip/logger/logger.cpp.o: /home/arrizza/projects/cpip/cpip/logger/logger.cpp
	c++ -MMD -c "-Isrc" "-I/home/arrizza/projects/cpip"  -std=c++20 -D_GNU_SOURCE  /home/arrizza/projects/cpip/cpip/logger/logger.cpp -o debug/jtestdft-dir/home/arrizza/projects/cpip/cpip/logger/logger.cpp.o

#-- jtestdft: build source files
jtestdft-build: debug/jtestdft-dir/src/main.cpp.o debug/jtestdft-dir/home/arrizza/projects/cpip/cpip/logger/logger.cpp.o 

debug/jtestdft: debug/jtestdft-dir/src/main.cpp.o debug/jtestdft-dir/home/arrizza/projects/cpip/cpip/logger/logger.cpp.o 
	c++ debug/jtestdft-dir/src/main.cpp.o debug/jtestdft-dir/home/arrizza/projects/cpip/cpip/logger/logger.cpp.o     -o debug/jtestdft

#-- jtestdft: link
jtestdft-link: debug/jtestdft

# used to pass args in cpp and gtest cmd lines
%:
	@:
#-- jtestdft: run executable
jtestdft-run: jtestdft-link
	@debug/jtestdft $(filter-out $@,$(MAKECMDGOALS))

#-- jtestdft: clean files in this target
jtestdft-clean:
	rm -f debug/jtestdft-dir/src/*.o
	rm -f debug/jtestdft-dir/src/*.d
	rm -f debug/jtestdft-dir/home/arrizza/projects/cpip/cpip/logger/*.o
	rm -f debug/jtestdft-dir/home/arrizza/projects/cpip/cpip/logger/*.d
	rm -f debug/jtestdft

#-- clean files
clean: jtestdft-clean 

help:
	@printf "Available targets:\n"
	@printf "  [32;01mall                                [0m build all\n"
	@printf "  [32;01mclean                              [0m clean files\n"
	@printf "  [32;01mhelp                               [0m this help info\n"
	@printf "  [32;01mjtestdft                           [0m build jtestdft\n"
	@printf "    [32;01mjtestdft-build                     [0m jtestdft: build source files\n"
	@printf "    [32;01mjtestdft-clean                     [0m jtestdft: clean files in this target\n"
	@printf "    [32;01mjtestdft-init                      [0m jtestdft: initialize for debug build\n"
	@printf "    [32;01mjtestdft-link                      [0m jtestdft: link\n"
	@printf "    [32;01mjtestdft-run                       [0m jtestdft: run executable\n"
	@printf "\n"
