# How to test jtestdft

## Test process

Run the following in order:

<ol>
<li><b>ensure xplat_utils is installed</b></li>

The first time after cloning this project, run:

```bash
# note: any changes done in xplat_utils will be deleted
# only do once!
./do_subm_update full
```

If there is an update to xplat_utils, use:

```bash
./do_subm_update
```

<li><b>remove out directory</b></li>

This step ensures the output directory has no stale data in it.

```bash
./do_clean full   # to clean out venv and pycaches
./do_clean
```

<br/><li><b>run do_install</b></li>

This step ensures additional utilities and python modules are installed correctly

```bash
./do_install full      # do this once and then periodically
./do_install
```

* **Expected**: no warnings or errors

<br/><li><b>run doxygen</b></li>

This step generates the low-level documentation

```bash
./do_doc
./do_check  # check Doxyfile content
```

* **Expected**: out/doxygen.txt is empty, has no warnings or errors
* **Expected**: the out/(project-name).pdf contains full low-level documentation

<br/><li><b>run static analysis</b></li>

This step runs various static analysis tests

```bash
# build needed for cppcheck lint
./do_gen     # gen makefile
./do_build   # build app

./do_lint
```

* **Expected**: no warnings or errors

<br/><li><b>run the project</b></li>

This step ensures the app runs correctly

```bash
./doit           # builds and runs debug for gcc
./doit debug     # same
./doit release   # builds and runs release for gcc
```

* **Expected**: no failures or other warnings, PASSED only

<br/><li><b>run verification</b></li>

```bash
./do_ver   # currently unimplemented
./do_ut    # currently unimplemented
```

* **Expected**: out/ver/summary.pdf shows all requirements tested and passed
* **Expected**: out/ver directory is created
* **Expected**: out/coverage/index.html shows high coverage, typically 95% or better

<br/><li><b>repeat these steps on the other OS platforms</b></li>

* **Expected**: all OS platforms should behave the same

</ol>
