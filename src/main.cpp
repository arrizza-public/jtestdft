#include <cmath>
#include <complex>
#include <iomanip>
#include <iostream>

// --------------------
//! show the results of using DFT on known sine wave signals
class Samples {
public:
    // --------------------
    //! constructor
    //! @param r   the sample rate (samples/second)
    //! @param l   the length (in seconds) of the sample
    Samples(double r, double l)
            : sample_rate(r), sample_length(l), num_samples((int) (r * l)) {

        // allocate the result and signal arrays
        result = new std::complex<double>[num_samples];
        sig = new double[num_samples];

        // initialize signal array
        for (int i = 0; i < num_samples; ++i) {
            sig[i] = 0.0;
        }
    }

    // --------------------
    //! add a sine wave to the sampling array
    //!
    //! @param frequency   the frequency of the sine wave
    //! @param ampl        the amplitide of the sine wave
    //! @param phase       the phase (0 to pi/2) of the sine wave
    //! @param bias        the bias if any of the sine wave
    void add_sine(double frequency, double ampl, double phase, double bias) const {
        if (sample_rate < 2.0 * frequency) {
            std::cout << "The sample_rate has to be at least 2 times the highest frequency in the signal"
                      << std::endl;
            return;
        }

        double const omega = 2.0 * M_PI * frequency;
        double const convertedPhase = phase / 2.0;

        // fill the signal array with samples of the sine wave
        for (int i = 0; i < num_samples; ++i) {
            // calculate the instantaneous time
            double const t = (double) i / sample_rate;

            sig[i] += bias + (ampl * ::sin((omega * t) + convertedPhase));
        }
    }

    // --------------------
    //! add a square wave to the samples
    //!
    //! @param frequency   the frequency of the square wave
    //! @param ampl        the amplitude of the square wave
    //! @param phase       the phase (0 to pi/2) of the square wave
    //! @param bias        the bias if any of the square wave
    void add_square(double frequency, double ampl, double phase, double bias) const {
        if (sample_rate < 2.0 * frequency) {
            std::cout << "The sample_rate has to be at 2 times the highest frequency in "
                         "the signal"
                      << std::endl;
            return;
        }

        double const fullwave = 1.0 / frequency;
        double const halfwave = fullwave / 2.0;

        for (int i = 0; i < num_samples; ++i) {
            double const t = (double) i / sample_rate;
            double const rem = ::fmod(t + phase, fullwave);
            if (rem < halfwave) {
                sig[i] += ampl + bias;
            } else {
                sig[i] += -ampl + bias;
            }
        }
    }

    // --------------------
    //! calculate the DFT for the samples
    void dft() const {
        double const arg = -1 * 2.0 * M_PI / (double) num_samples;
        for (int f = 0; f < num_samples; f++) {
            for (int k = 0; k < num_samples; k++) {
                if (f == 0 || k == 0) {
                    result[f] += sig[k] * std::complex<double>(1.0, 0.0);
                } else {
                    result[f] += sig[k] * std::complex<double>(cos(k * f * arg), sin(k * f * arg));
                }
            }

            result[f] /= num_samples;
        }
    }

    // --------------------
    //! the calculated bias of the signal(s) found in the samples
    //!
    //! @return the bias of the signal(s)
    [[nodiscard]] auto bias() const -> double {
        return result[0].real();
    }

    // --------------------
    //! the calculated frequency of the ith wave found in the samples
    //!
    //! @param i  the ith wave
    //! @return the frequency of the ith wave
    [[nodiscard]] auto frequency(int i) const -> double {
        return (i * sample_rate) / num_samples;
    }

    // --------------------
    //! the calculated amplitude of the ith wave found in the samples
    //!
    //! @param i  the ith wave
    //! @return the amplitude of the ith wave
    [[nodiscard]] auto amplitude(int i) const -> double {
        return 2.0 * ::sqrt((result[i].real() * result[i].real()) + (result[i].imag() * result[i].imag()));
    }

    // --------------------
    //! the calculated phase of the ith wave found in the samples
    //!
    //! @param i  the ith wave
    //! @return the phase of the ith wave
    [[nodiscard]] auto phase(int i) const -> double {
        return (2.0 * ::atan(result[i].imag() / result[i].real())) + M_PI;
    }

    // these are all public for convenience
    //! the sample rate (samples / second)
    const double sample_rate; // samples / second
    //! the total sample_length (in seconds)
    const double sample_length;
    //! the total number of samples
    const int num_samples;

    //! the array of signals
    double *sig;
    //! the array of results
    std::complex<double> *result;
};

// --------------------
auto main() -> int {
    // add empty samples with a sample rate of 1024/second and length of 1/2
    // second this should end up with 512 samples in all
    Samples const s(1024.0, 0.5);

    // add sine waves to the samples
    //            freq   ampl  ph   bias
    s.add_sine(110.0, 10.0, 1.1, 0.1);
    s.add_sine(220.0, 20.0, 1.2, 0.1);
    s.add_sine(330.0, 30.0, 1.3, 0.1);
    s.add_sine(440.0, 40.0, 4.4, 0.1);

    // 90 out of phase, so it's a cosine
    // s.add_sine(100.0, 10.0, 3.1416, 0.1);

    // add square waves
    //               freq   ampl      ph   bias
    // s.add_square(10.0,  2.0, 0.0, 0.0);
    // s.add_square(10.0,  99.07820, 0.0, 0.0);
    // s.add_square(11.0,  99.03134, 0.0, 0.0);
    // s.add_square(12.0,  99.00000, 0.0, 0.0);
    // s.add_square(13.0,  99.00000, 0.0, 0.0);

    // at this point, the samples are in the sig array
    // calculate the dft from those signals
    s.dft();

    // should report the same frequencies, amplitudes and phases as added above
    std::cout << "sample_rate = " << s.sample_rate << std::endl;
    std::cout << "num_samples = " << s.num_samples << std::endl;
    std::cout << "bias: " << s.bias() << std::endl;
    for (int i = 1; i < s.num_samples / 2; ++i) {
        double const ampl = s.amplitude(i);
        if (ampl > 0.00001) {
            std::cout << setiosflags(std::ios::fixed) << "freq: " << std::setw(7) << std::setprecision(2)
                      << s.frequency(i) << "  ampl: " << std::setw(8) << std::setprecision(4) << ampl
                      << "  phase: " << s.phase(i) << std::endl;
        }
    }

    return 0;
}
