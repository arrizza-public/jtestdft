from pyalamake import alamake

# === cpip
pkg = alamake.find_package('cpip.logger')

# === C++ tgt
tgt = alamake.create('jtestdft', 'cpp')
tgt.add_sources([
    'src/main.cpp',
])
tgt.add_include_directories(['src'])
tgt.add_sources(pkg.src)
tgt.add_include_directories(pkg.include_dir)

# === gtest
# ut = alamake.create('ut', 'gtest')
#
# # add homebrew dirs if running on macOS
# ut.add_homebrew()
#
# ut.add_include_directories([
#     'src',
# ])
# ut.add_sources([
#     'src/main.cpp',
# 
#     'ut/ut_test1.cpp',
# ])
# ut.add_link_libraries('gtest_main')
# ut.add_coverage(['src'])
# ut.add_sources(pkg.src)
# ut.add_include_directories(pkg.include_dir)

# === generate makefile for all targets
alamake.makefile()
