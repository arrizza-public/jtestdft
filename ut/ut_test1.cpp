#include "../src/version.h"
#include <gtest/gtest.h>

// ---------------------
TEST(test1, version)
{
    ASSERT_STREQ("0.0.1", version);
}

// TODO any others?
// // ---------------------
// TEST(test1, something_true) {
//     bool val = do_something_true();
//     ASSERT_TRUE(val);
//
//     val = do_something_false();
//     ASSERT_FALSE(val);
// }

// ---------------------
int main(int argc, char** argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}